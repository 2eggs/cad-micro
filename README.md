# CAD-micro

> CAD 微服务

## 接口名

### 模糊查找公司列表


queryCompanyListByCompanyName
```
param:
    CompanyName: String!

return:
    CompanyName: String,
    CompanyID: String
```

## 融资事件详情界面接口设计

### 公司概览 

queryCompanyOverviewByCompanyID

```
param：
    CompanyID:String!

return：
    City: String,`gorm:"type:varchar(255);"`
    FoundingRounds: Int,
    FirstFunded { 
        FirstFundedAt: String,
        FundingRoundType: String,
        RaisedAmountUSD: Int
    },
    LastFunded {
        FirstFundedAt: String,
        FundingRoundType: String,
        RaisedAmountUSD: Int
    },
    FundingTotalUSD
```

###  融资交易  (需要实现分页)

queryFinancingListByCompanyID

```
param: 
    first
    after
    last
    before
    CompanyID:String!
return:
    Financing[] {
        FundingRoundType,
        FundedAt,
        RaisedAmountUSD,
        Invertor[] {
            InvestorName,
            InvestorID,

        }
    }   
```


### 投资交易   

queryInvestmentListByCompanyID

```
params:
    first
    after
    last
    before
    CompanyID:String!
return:
    Investment[] {
        
        {
            CompanyName,
            CompanyID,
            CompanyCategoryCode,
        }
        {
            FundingRoundType
            FundedAt,
        }
        
    }
    
```

### 对外收购   

queryExternalAcquisitionListByCompanyID  

```
params:
    first
    after
    last
    before
    CompanyID
return:
    ExternalAcquisition[] {
        AcquiredAt,
        CompanyID,
        CompanyName,
        CompanyCategoryCode,
        PriceAmount,
    }
    
```

### 兼并信息

queryMergerInformationByCompanyID

```
params:
    CompanyID
return:
    AcquirerID,
    AcquirerName,
    AcquiredAt,
    PriceAmount,
```

## 融资事件详情界面接口

### 融资事件概览
queryFinancingOverviewByFinancingIndex

```
param：
    FinancierID: String!
    FundingRoundType: String!
    FundedAt: String!
    RaisedAmountUSD: Int!
return：
    CompanyName: String
    CompanyID:  String
    FundingRoundType:String
    FundedAt: String
    RaisedAmountUSD: Int

```

### 融资事件投资方

queryFinancingInvestorListByFinancingIndex

```
params:
    FinancierID: String!
    FundingRoundType: String!
    FundedAt: String!
    RaisedAmountUSD: Int!
return:
    Investor[] {
        InvestCompanyName,
        InvestCompanyID,
        InvestCompanyCategoryCode
    }
```

### 融资方其它交易

接口复用 queryFinancingListByCompanyID

### 数据模型   
