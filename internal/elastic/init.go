package elastic

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/mayunmeiyouming/cad-micro/internal/models"
)

var client *elasticsearch.Client

func init() {
	var err error
	config := elasticsearch.Config{}
	config.Addresses = []string{"http://127.0.0.1:9200"}
	client, err = elasticsearch.NewClient(config)
	checkError(err)
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// Investor ...
type Investor struct {
	CompanyID    string
	CompanyName  string
	CategoryCode string
}

var a = []int{1, 2}

// var companies = []Investor{
// 	Investor{CompanyID: "1", CompanyName: "黄伟"},
// 	Investor{CompanyID: "2", CompanyName: "每年"},
// 	Investor{CompanyID: "3", CompanyName: "模拟"},
// 	Investor{CompanyID: "4", CompanyName: "美女"},
// 	Investor{CompanyID: "5", CompanyName: "末年"},
// }

// CreateIndex ...
func CreateIndex() {

	body := map[string]interface{}{
		"mappings": map[string]interface{}{
			"properties": map[string]interface{}{
				"companyID": map[string]interface{}{
					"type":  "keyword",
					"store": true,
				},
				"companyName": map[string]interface{}{
					"type":  "keyword",
					"store": true,
					"index": true,
					// "analyzer": "standard",
				},
				"categoryCode": map[string]interface{}{
					"type":  "keyword",
					"store": true,
				},
			},
		},
	}
	jsonBody, _ := json.Marshal(body)
	fmt.Println(string(jsonBody))
	req := esapi.IndicesCreateRequest{
		Index: "companies",
		Body:  bytes.NewReader(jsonBody),
	}
	res, err := req.Do(context.Background(), client)
	checkError(err)
	defer res.Body.Close()
	fmt.Println(res.String())
}

// InsertBatch ...
func InsertBatch() {
	var bodyBuf bytes.Buffer

	database, err := gorm.Open("mysql", "newcomer:newcomer@(47.112.200.141:3306)/training_camp?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("connection succedssed")

	var companies []models.Companies
	// 数据库连接已经封装在Database的Client
	err = database.Find(&companies).Error

	for i, value := range companies {
		createLine := map[string]interface{}{
			"create": map[string]interface{}{
				"_index": "companies",
				"_id":    i,
				// "_type":  "test_type",
			},
		}
		jsonStr, _ := json.Marshal(createLine)
		bodyBuf.Write(jsonStr)
		bodyBuf.WriteByte('\n')

		body := map[string]interface{}{
			"companyID":    value.Permalink,
			"companyName":  value.Name,
			"categoryCode": value.CategoryCode,
		}
		jsonStr, _ = json.Marshal(body)
		bodyBuf.Write(jsonStr)
		bodyBuf.WriteByte('\n')
	}

	req := esapi.BulkRequest{
		Body: &bodyBuf,
	}
	res, err := req.Do(context.Background(), client)
	checkError(err)
	defer res.Body.Close()
	fmt.Println(res.String())
}

// Column ...
type Column struct {
	Name string
	Type string
}

// ESSelect ...
type ESSelect struct {
	Columns []Column
	Rows    [][]string
}

// SelectBySQL ...
func SelectBySQL(name string) (*ESSelect, error) {
	var sql = "select * from companies as c where c.companyName like '" + name + "%' limit 10"
	// var sql = "select * from companies as c where c.companyName = " + name + " limit 10"
	query := map[string]interface{}{
		"query": sql,
	}

	// query := map[string]interface{}{
	// 	"query": map[string]interface{}{
	// 		"match": map[string]interface{} {
	// 			"companyName": name,
	// 		},
	// 	},
	// }

	jsonBody, _ := json.Marshal(query)
	fmt.Println(string(jsonBody))

	req := esapi.SQLQueryRequest{
		Body: bytes.NewReader(jsonBody),
	}
	res, err := req.Do(context.Background(), client)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	fmt.Println(res.String()[9:])
	var a ESSelect
	fmt.Println()
	json.Unmarshal([]byte(res.String()[9:]), &a)
	fmt.Println(a)
	return &a, nil
}

// DeleteIndex ...
func DeleteIndex() {
	req := esapi.IndicesDeleteRequest{
		Index: []string{"companies"},
	}
	res, err := req.Do(context.Background(), client)
	checkError(err)
	defer res.Body.Close()
	fmt.Println(res.String())
}
