package server

import (
	context "context"
	"errors"
	"log"
	"strconv"

	"github.com/golang/protobuf/ptypes/wrappers"
	// "gitlab.com/mayunmeiyouming/cad-micro/internal/models"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

// QueryExternalAcquisitionListByCompanyID 通过公司ID查找对外收购的信息
func (s *Server) QueryExternalAcquisitionListByCompanyID(ctx context.Context, request *cad.QueryExternalAcquisitionListByCompanyIDRequest) (*cad.ExternalAcquisitionConnection, error) {

	if acquisitions, _ := s.Repository.QueryCompanyInfoByCompanyID(request.CompanyId); acquisitions == nil {
		return nil, errors.New("company does not exist?")
	}

	externalAcquisitionConn := &cad.ExternalAcquisitionConnection{}

	totalCount, err := s.Repository.QueryAcquisitionsCountByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}
	externalAcquisitionConn.TotalCount = int64(*totalCount)
	if *totalCount <= 0 {
		log.Println("没有收购")
		return nil, errors.New("没有收购信息")
	}

	log.Println("收购总数: ", *totalCount)

	// 将first, after, last, before转换成limit，offset
	limit := 0
	offset := 0
	if request.First != nil && request.After != nil {
		if request.First.Value <= 0 || request.After.Value < 0 || request.After.Value >= int64(*totalCount) {
			log.Println("请设置合理的范围")
			return nil, errors.New("请设置合理的范围")
		}
		offset = int(request.After.Value)
		if request.First.Value+request.After.Value > int64(*totalCount) {
			limit = *totalCount - int(request.After.Value)
		} else {
			limit = int(request.First.Value)
		}
	} else if request.Last != nil && request.Before != nil {
		if request.Last.Value <= 0 || request.Before.Value < 0 || request.Before.Value >= int64(*totalCount) {
			log.Println("请设置合理的范围")
			return nil, errors.New("请设置合理的范围")
		}
		limit = int(request.Before.Value)
		if request.Before.Value-request.Last.Value < 0 {
			offset = 0
		} else {
			offset = int(request.Before.Value - request.Last.Value + 1)
		}
	}

	log.Println("Limit: ", limit)
	log.Println("Offset: ", offset)

	//分页数据处理
	pageInfo := &cad.PageInfo{}
	

	externalAcquisitionConn.TotalCount = int64(*totalCount)
	count := *totalCount
	if offset > 0 {
		pageInfo.HasPreviousPage = true
	}

	if offset < count {
		pageInfo.HasNextPage = true
	}

	pageInfo.StartCursor = &wrappers.StringValue{Value: strconv.Itoa(offset)}

	pageInfo.EndCursor = &wrappers.StringValue{Value: strconv.Itoa(limit + offset - 1)}

	externalAcquisitionConn.PageInfo = pageInfo

	acquisitionDB, err := s.Repository.QueryAcquisitionsByCompanyID(request.CompanyId, limit, offset)

	//遍历数据
	for i, acquisition := range acquisitionDB {
		node := &cad.ExternalAcquisition{
			CompanyInfo: &cad.CompanyInfo{
				CompanyName:  acquisition.CompanyName,
				CompanyId:    acquisition.CompanyPermalink,
				CategoryCode: acquisition.CompanyCategoryCode,
			},
			AcquiredAt:  acquisition.AcquiredAt,
			PriceAmount: acquisition.PriceAmount,
		}
		externalAcquisitionConn.Nodes = append(externalAcquisitionConn.Nodes, node)

		acquisitionEdge := &cad.ExternalAcquisitionEdge{
			Node: &cad.ExternalAcquisition{
				CompanyInfo: &cad.CompanyInfo{
					CompanyName:  acquisition.CompanyName,
					CompanyId:    acquisition.CompanyPermalink,
					CategoryCode: acquisition.CompanyCategoryCode,
				},
				AcquiredAt:  acquisition.AcquiredAt,
				PriceAmount: acquisition.PriceAmount,
			},
			Cursor: strconv.Itoa(offset + i),
		}
		externalAcquisitionConn.Edges = append(externalAcquisitionConn.Edges, acquisitionEdge)
	}

	return externalAcquisitionConn, nil
}

//ChangeFirstAfterToLimitOffset ...
func (s *Server) getLimitAndOffsetByFirstAfter(First *wrappers.Int64Value, After *wrappers.Int64Value) (limit, offset int) {
	limit = int(First.Value)
	if After != nil {
		offset = int(After.Value)
	} else {
		offset = 0
	}
	return
}

// ChangeLastBeforeToLimitOffset ...
func (s *Server) getLimitAndOffsetByLastBefore(Last *wrappers.Int64Value, Before *wrappers.Int64Value, request *cad.QueryExternalAcquisitionListByCompanyIDRequest) (limit, offset int) {
	limit = int(Last.Value)

	var count int
	length, err := s.Repository.QueryAcquisitionsCountByCompanyID(request.CompanyId)
	if err != nil {
		log.Println(err)
		return 0, 0
	}
	count = *length

	if Before != nil {
		cur := limit + int(Before.Value)
		offset = count - cur
	} else {
		offset = count - limit
	}
	return
}
