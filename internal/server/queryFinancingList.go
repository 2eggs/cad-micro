package server

import (
	context "context"
	"errors"
	"fmt"
	"strconv"

	// "gitlab.com/mayunmeiyouming/cad-micro/internal/models"

	"github.com/golang/protobuf/ptypes/wrappers"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

//QueryFinancingListByCompanyID 通过公司id查询公司的所有融资事件
func (s *Server) QueryFinancingListByCompanyID(ctx context.Context, request *cad.QueryFinancingListByCompanyIDRequest) (*cad.FinancingDealConnection, error) {
	var limit, offset, count int
	length, err := s.Repository.QueryFinancingEventCountByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}
	count = *length
	if count == 0 {
		return nil, errors.New("no datas")
	}
	if request.First != nil && request.Last != nil {
		return nil, errors.New("param first and last can't exist in same time?")
	}

	//把first和after的值转为limit和offset
	if request.First != nil {
		limit, offset = s.ChangeFirstAfterToLimitOffset(request.First, request.After)
	}

	//把last和before的值转为limit和offset
	if request.Last != nil {
		limit, offset = s.ChangeLastBeforeToLimitOffset(request.Last, request.Before, count)
	}

	companyRounds, err := s.Repository.QueryFinancingListByCompanyID(request.CompanyId, limit, offset)
	if err != nil {
		return nil, err
	}

	financingDealConn := &cad.FinancingDealConnection{}
	pageInfo := &cad.PageInfo{}

	if offset > 1 {
		pageInfo.HasPreviousPage = true
	} else {
		pageInfo.HasPreviousPage = false
	}

	if offset < count && offset+limit < count {
		pageInfo.HasNextPage = true
	} else {
		pageInfo.HasNextPage = false
	}
	pageInfo.StartCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + 1)}
	pageInfo.EndCursor = &wrappers.StringValue{Value: strconv.Itoa(offset + limit)}

	financingDealConn.PageInfo = pageInfo

	financingDealConn.TotalCount = int64(count)

	for i, companyRound := range companyRounds {

		//查找投资方列表
		investmentList, err := s.Repository.QueryInvestListByCompanyIDAndFundedAt(request.CompanyId, companyRound.FundedAt)
		if err != nil {
			return nil, err
		}
		var investorList []*cad.CompanyInfo
		for _, investment := range investmentList {
			investorList = append(investorList,
				&cad.CompanyInfo{
					CompanyId:    investment.InvestorPermalink,
					CompanyName:  investment.InvestorName,
					CategoryCode: investment.InvestorCategoryCode,
				})
		}

		node := &cad.FinancingDeal {
			FinancingInfo: &cad.FinancingInfo{
				CompanyId:        companyRound.CompanyPermalink,
				FundingRoundType: companyRound.FundingRoundType,
				FundedAt:         companyRound.FundedAt,
				RaisedAmountUsd:  int64(companyRound.RaisedAmountUsd),
			},
			Investors: investorList,
		}
		financingDealConn.Nodes = append(financingDealConn.Nodes, node)

		edge := &cad.FinancingDealEdge{
			Node: &cad.FinancingDeal{
				FinancingInfo: &cad.FinancingInfo{
					CompanyId:        companyRound.CompanyPermalink,
					FundingRoundType: companyRound.FundingRoundType,
					FundedAt:         companyRound.FundedAt,
					RaisedAmountUsd:  int64(companyRound.RaisedAmountUsd),
				},
				Investors: investorList,
			},
			Cursor: strconv.Itoa(offset + i + 1),
		}
		financingDealConn.Edges = append(financingDealConn.Edges, edge)
	}
	return financingDealConn, nil
}

//ChangeFirstAfterToLimitOffset ...
func (s *Server) ChangeFirstAfterToLimitOffset(First *wrappers.Int64Value, After *wrappers.Int64Value) (limit, offset int) {
	limit = int(First.Value)
	if After != nil {
		offset = int(After.Value)
	} else {
		offset = 0
	}
	return
}

//ChangeLastBeforeToLimitOffset ...
func (s *Server) ChangeLastBeforeToLimitOffset(Last *wrappers.Int64Value, Before *wrappers.Int64Value, count int) (limit, offset int) {
	limit = int(Last.Value)
	if Before != nil {
		offset := int(Before.Value)
		offset = offset - limit
	} else {
		offset = count - limit
	}
	fmt.Println(limit, offset)
	return
}
