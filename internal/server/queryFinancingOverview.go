package server

import (
	context "context"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

// QueryFinancingOverviewByFinancingIndex ...
func (s *Server) QueryFinancingOverviewByFinancingIndex(
	ctx context.Context, 
	request *cad.QueryFinancingOverviewByFinancingIndexRequest) (*cad.FinancingOverviewResponse, error) {
	
	FundedInfo, err := s.Repository.QueryFundedInfoByFinancingIndex(
		request.CompanyId, 
		request.FundingRoundType, 
		request.FundedAt, 
		int(request.RaisedAmountUsd))

	if err != nil {
		return nil, err
	}

	companyInfo := cad.CompanyInfo {
		CompanyId: FundedInfo.CompanyPermalink,
		CompanyName: FundedInfo.CompanyName,
		CategoryCode: FundedInfo.CompanyCategoryCode,
	}

	fundedInfo := cad.FinancingInfo {
		CompanyId: FundedInfo.CompanyPermalink,
		FundingRoundType: FundedInfo.FundingRoundType,
		FundedAt: FundedInfo.FundedAt,
		RaisedAmountUsd: int64(FundedInfo.RaisedAmountUsd),
	}

	res := &cad.FinancingOverviewResponse {
		CompanyInfo: &companyInfo,
		FinancingInfo: &fundedInfo,
	}

	return res, nil
}

