package server

import (
	context "context"
	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
	"gitlab.com/mayunmeiyouming/cad-micro/internal/elastic"
)

// QueryCompanyListByCompanyName 该接口用于搜索页的模糊查找，通过输入的公司名查找拥有相同的字的公司信息
func (s *Server) QueryCompanyListByCompanyName(ctx context.Context, request *cad.QueryCompanyListByCompanyNameRequest) (*cad.CompanyInfoResponse, error) {
	// companyList, err := s.Repository.QueryCompanyListByCompanyName(request.CompanyName)
	// if err != nil {
	// 	return nil, err
	// }

	response, err := elastic.SelectBySQL(request.CompanyName)
	if err != nil {
		return nil, err
	}

	res := &cad.CompanyInfoResponse {
		CompanyInfo: make([]*cad.CompanyInfo, 0),
	}
	for _, value := range response.Rows {
		companyInfo := &cad.CompanyInfo {
			CompanyId: value[1],
			CompanyName: value[2],
			CategoryCode: value[0],
		}
		res.CompanyInfo = append(res.CompanyInfo, companyInfo)
	}
	return res, nil
}