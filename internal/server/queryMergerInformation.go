package server

import (
	context "context"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

//QueryMergerInformationByCompanyID 兼并信息
func (s *Server) QueryMergerInformationByCompanyID(ctx context.Context, request *cad.QueryMergerInformationByCompanyIDRequest) (*cad.MergerInfoResponse, error) {
	//获取公司信息数据
	_, err := s.Repository.QueryCompanyInfoByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}

	//获取收购信息数据
	acquisitions, err := s.Repository.QueryAcquisitionsInfoByCompanyID(request.CompanyId)
	if err != nil {
		return nil, err
	}

	//获取的信息进行汇总返回
	merge := &cad.MergerInfoResponse{
		AcquirerInfo: &cad.CompanyInfo{
			CompanyName:  acquisitions.AcquirerName,
			CompanyId:    acquisitions.AcquirerPermalink,
			CategoryCode: acquisitions.AcquirerCatagoryCode,
		},
		AcquiredAt:  acquisitions.AcquiredAt,
		PriceAmount: acquisitions.PriceAmount,
	}

	return merge, nil
}
