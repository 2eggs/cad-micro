package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	wrapperspb "github.com/golang/protobuf/ptypes/wrappers"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	investorListConnection, err := client.QueryFinancingInvestorListByFinancingIndex(
		context.Background(),
		&cad.QueryFinancingInvestorListByFinancingIndexRequest {
			First: &wrapperspb.Int64Value {
				Value: 5,
			},
			After: &wrapperspb.Int64Value {
				Value: 0,
			},
			Last: nil,
			Before: nil,
			CompanyId: "/company/waywire",  
			FundingRoundType: "series-a",
			FundedAt: "2012-06-30",
			RaisedAmountUsd: 1750000,
		},
	)

	if err != nil {
		log.Debug(err)
	}

	investorListConnectionJson, err := json.MarshalIndent(investorListConnection, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("investorListConnection:")
	fmt.Println(string(investorListConnectionJson))
	fmt.Println("==================================================")
}