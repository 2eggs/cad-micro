package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	mergerInfo, err := client.QueryMergerInformationByCompanyID(
		context.Background(),
		&cad.QueryMergerInformationByCompanyIDRequest{
			CompanyId: "/company/actividentity",
		},
	)

	if err != nil {
		log.Debug(err)
	}

	mergerInfoJson, err := json.MarshalIndent(mergerInfo, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("MergerInformation:")
	fmt.Println(string(mergerInfoJson))
	fmt.Println("==================================================")
}
