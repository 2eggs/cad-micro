package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	//"log"

	//"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/prometheus/common/log"
	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	acquisitionInfo, err := client.QueryExternalAcquisitionListByCompanyID(
		context.Background(),
		&cad.QueryExternalAcquisitionListByCompanyIDRequest{
			// First: &wrappers.Int64Value{Value: 2},
			// After:     &wrappers.Int64Value{Value: 1},
			Last:      &wrappers.Int64Value{Value: 1},
			Before:    &wrappers.Int64Value{Value: 1},
			CompanyId: "/company/cisco",
		},
	)

	if err != nil {
		log.Debug(err)
	}

	acquisitionInfoJson, err := json.MarshalIndent(acquisitionInfo, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println("acquisitionInfo:")
	fmt.Println(string(acquisitionInfoJson))
	fmt.Println("==================================================")
}
