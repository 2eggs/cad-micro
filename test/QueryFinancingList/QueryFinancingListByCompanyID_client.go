package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/prometheus/common/log"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

func main() {
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})
	companiesRoundsConn, err := client.QueryFinancingListByCompanyID(
		context.Background(),
		&cad.QueryFinancingListByCompanyIDRequest{
			CompanyId: "/company/121nexus",
			Last:      &wrappers.Int64Value{Value: 2},
			Before:    &wrappers.Int64Value{Value: 2},
		},
	)
	if err != nil {
		log.Debug(err)
	}
	companiesRoundsConnJSON, err := json.MarshalIndent(companiesRoundsConn, "", "\t")
	if err != nil {
		log.Debug(err)
	}
	fmt.Println(string(companiesRoundsConnJSON))
}
